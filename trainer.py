import os
import torch
import argparse
import datetime

from glob import glob
from omegaconf import OmegaConf as OC
from pytorch_lightning import Trainer, loggers
from pytorch_lightning.callbacks import ModelCheckpoint

from lightning_model import ACMSV


def main(args):
    hparams = OC.load(args.config)
    now = datetime.datetime.now().strftime("%m_%d_%H")
    hparams.name = f'global_{now}'

    os.makedirs(hparams.log.tensorboard_dir, exist_ok=True)
    model = ACMSV(hparams)

    ckpt_path = f'sv_acm_{now}_{{epoch}}'
    checkpoint_callback = ModelCheckpoint(
        filepath=os.path.join(hparams.log.checkpoint_dir, ckpt_path),
        verbose=True,
        save_top_k=5,
        monitor='eer',
        mode='min',
        prefix='',
    )

    trainer = Trainer(
        check_val_every_n_epoch=180,
        gpus=1,
        max_epochs=1000000,
        checkpoint_callback=checkpoint_callback,
        logger=model.tblogger,
        progress_bar_refresh_rate=4,
        gradient_clip_val=1.0,
        resume_from_checkpoint=args.checkpoint_path,
    )
    trainer.fit(model)


if __name__ =='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="Configuration yaml file")
    parser.add_argument('-p', '--checkpoint_path', type=str, required=False,
                        help="Checkpoint path for resuming")
    args = parser.parse_args()
    main(args)
