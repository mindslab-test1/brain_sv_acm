FROM docker.maum.ai:443/jun3518:base-20.07.22
RUN python3 -m pip --no-cache-dir install --upgrade \
        tensorboard==2.0.0 \
        gpustat==0.6.0 \
        grpcio==1.13.0 \
        grpcio-tools==1.13.0 \
        protobuf==3.6.0 \
        slackclient==2.7.3 \
        tensorboard-reporter==0.0.3 \
        && \
apt update && \
apt install -y \
    tmux \
    htop \
    ncdu && \
apt clean && \
apt autoremove && \
rm -rf /var/lib/apt/lists/* /tmp/* && \
mkdir /root/sv_acm
COPY . /root/sv_acm
RUN cd /root/sv_acm/ && \
    python3 -m grpc.tools.protoc --proto_path=brain_idl/protos/audio/legacy --python_out=. --grpc_python_out=. dap.proto
