import grpc
import argparse
import google.protobuf.empty_pb2 as empty

import dap_pb2
from dap_pb2_grpc import GhostVLADStub


class SVACMClient(object):
    def __init__(self, remote='127.0.0.1:43001', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = GhostVLADStub(channel)
        self.chunk_size = chunk_size

    def get_dvector_from_wav(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.GetDvectorFromWav(wav_binary)

    def get_emb_config(self):
        return self.stub.GetEmbConfig(empty.Empty())

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield dap_pb2.WavBinary(bin=wav_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='ACM based speaker verification client')
    parser.add_argument('-r', '--remote', type=str, default='127.0.0.1:43001',
                        help='grpc: ip:port')
    parser.add_argument('-i', '--input', type=str, required=True,
                        help='input wav file')
    args = parser.parse_args()

    client = SVACMClient(args.remote)

    with open(args.input, 'rb') as rf:
        wav_binary = rf.read()
    
    dvector_result = client.get_dvector_from_wav(wav_binary).data
    print(dvector_result)

    emb_dim = client.get_emb_config().emb_dim
    print(emb_dim)
