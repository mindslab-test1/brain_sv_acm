import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import pytorch_lightning as pl

from collections import OrderedDict
from sklearn.metrics import roc_curve
from scipy.optimize import brentq
from scipy.interpolate import interp1d

import dataloader

from model.cross_entropy import CrossEntropyLoss
from model.angle_proto import AngleProtoLoss
from model.ResNetACM import ResNetACM34L


class ACMSV(pl.LightningModule):
    def __init__(self, hparams, train=True):
        super(ACMSV, self).__init__()
        self.save_hyperparameters(hparams)
        self.loss = AngleProtoLoss(self.hparams)
        self.s_model = ResNetACM34L(nOut=self.hparams.proto.nOut)

        self.global_proto = nn.Parameter(torch.randn(self.hparams.data.speakers, self.hparams.proto.nOut))
        self.w = nn.Parameter(torch.tensor(10.))
        self.b = nn.Parameter(torch.tensor(-0.5))
        if train:
            from utils import aug
            from utils.tblogger import TensorBoardLoggerExpanded
            self.tblogger = TensorBoardLoggerExpanded(self.hparams.log.tensorboard_dir, self.hparams)
            self.globalce = CrossEntropyLoss(self.hparams.data.speakers,
                                             epsilon=self.hparams.proto.smoothing,
                                             label_smooth=True,
                                             eps_min=self.hparams.proto.eps_min,
                                             eps_max=self.hparams.proto.eps_max)
            self.compaug = aug.Comp(0.05, 0.5, 0.1)

    def forward(self, x):
        if self.training:
            x = self.compaug(x)
        x, orth = self.s_model(x)
        if self.training:
            gl = self.w * F.linear(
                    x,
                    F.normalize(self.global_proto if self.current_epoch > 90 else self.global_proto.detach(), dim=1)) + self.b
            return x, orth, gl
        else:
            return x, orth

    def inference(self, x):
        x, _ = self(x)
        x = x.squeeze(0)
        return x

    def eer(self, dists, labels):
        fpr, tpr, thres = roc_curve(labels, dists, pos_label=1)
        eer = torch.tensor(
            brentq(lambda x: 1. - x - interp1d(fpr, tpr)(x), 0., 1.))
        thr = torch.tensor(interp1d(fpr, thres)(eer))
        return eer, thr

    def training_step(self, batch, batch_nb):
        train_spec, label = batch
        y_hat, loss_orth, gl = self(train_spec)
        ml, acc = self.loss(y_hat)
        #loss += self.hparams.acm.lambd * loss_orth
        gcl = self.hparams.globalc.lambd * self.globalce(gl,label)
        ngp = F.normalize(self.global_proto,dim=1)
        gpl = self.hparams.globalc.beta * torch.matmul(ngp, ngp.t()).sum()
        loss = ml + gcl + gpl
        tensorboard_logs = {
            'loss': loss,
            'ml': ml,
            'gcl': gcl,
            'gpl': gpl,
            'acc': acc,
            #'eps': self.loss.criterion.epsilon,
            #'g_eps': self.globalce.epsilon,
            'g_acc': self.loss.accuracy(gl, label)
        }

        return {'loss': loss, 'log': tensorboard_logs}

    @torch.no_grad()
    def training_epoch_end(self, outputs):
        self.loss.criterion.epsilon += (
            self.loss.criterion.epsilon > self.hparams.proto.eps_max
        ) * (-self.hparams.proto.reset_margin) + (
            self.loss.criterion.epsilon <
            self.hparams.proto.eps_min) * (self.hparams.proto.reset_margin)
        self.globalce.epsilon += (
            self.globalce.epsilon > self.hparams.proto.eps_max
        ) * (-self.hparams.proto.reset_margin) + (
            self.globalce.epsilon <
            self.hparams.proto.eps_min) * (self.hparams.proto.reset_margin)
        if self.current_epoch % 45 == 0 and self.current_epoch > 0:
            self.tblogger.reporter()
        return {}

    def validation_step(self, batch, batch_nb):
        label, wav1, wav2 = batch
        out1, _ = self(wav1)
        out2, _ = self(wav2)
        dist = F.linear(out1, out2)
        return {'distance': dist, 'label': label}

    def validation_epoch_end(self, outputs):
        labels = torch.cat([x['label'] for x in outputs], dim=0).tolist()
        dists = torch.cat([x['distance'] for x in outputs], dim=0).tolist()
        eer, thr = self.eer(dists, labels)
        tensorboard_logs = {'eer': eer, 'threshold': thr}
        return {'eer': eer, 'threshold': thr, 'log': tensorboard_logs}

    def test_step(self, batch, batch_nb):
        label, wav1, wav2 = batch
        out1, _ = self(wav1)
        out2, _ = self(wav2)
        dist = F.linear(out1, out2)
        return {'distance': dist, 'label': label}

    #need mod
    def test_epoch_end(self, outputs):
        labels = torch.cat([x['label'] for x in outputs], dim=0).tolist()
        dists = torch.cat([x['distance'] for x in outputs], dim=0).tolist()
        eer, thr = self.eer(dists, labels)
        print(eer, thr)
        return {'eer': eer, 'threshold': thr}

    def weights(self):
        for k, v in self.named_parameters():
            if ("t_" not in k):
                yield v

    def configure_optimizers(self):
        weight_opt = torch.optim.Adam(
            self.weights(),
            lr=self.hparams.train.lr,
            weight_decay=self.hparams.train.weight_decay)
        return weight_opt

    def train_dataloader(self):
        return dataloader.create_vox_proto_dataloader(self.hparams)

    def val_dataloader(self):
        return dataloader.create_vox_verif_dataloader(self.hparams)

    def test_dataloader(self):
        return dataloader.create_vox_verif_dataloader(self.hparams)
