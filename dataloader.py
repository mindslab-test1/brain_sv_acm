import os
import torch
import random
import librosa
from glob import glob
from torch.utils.data import Dataset, DataLoader


def create_vox_proto_dataloader(hparams):
    def collate_fn(batch):
        wav_list = []
        spk_list = []
        for train_wav, ind in batch:
            wav_list.append(train_wav)
            spk_list.extend(ind)
        wav_list = torch.cat(wav_list, dim=0)
        spk_list = torch.tensor(spk_list)
        return wav_list, spk_list
    return DataLoader(dataset=Voxceleb_Proto_Dataset(hparams),
                  batch_size=hparams.proto.step,
                  shuffle=True,
                  num_workers=hparams.train.num_workers,
                  collate_fn=collate_fn,
                  pin_memory=True,
                  drop_last=True,
                  sampler=None)


class Voxceleb_Proto_Dataset(Dataset):
    def __init__(self, hparams):
        self.hparams = hparams
        self.directory = hparams.data.directory
        self.dataformat = hparams.data.wav_format

        #voxceleb hierarchy
        # voxceleb2(or 1)/id###/####/###.wav
        def _get_datalist(file_format, spk_list):
            _dl = []
            _spkl = []
            for i, spk in enumerate(spk_list): 
                _spk_dl_tmp = sorted(glob(os.path.join(spk, file_format)))
                _len = len(_spk_dl_tmp)
                if _len == 0:
                    continue
                _spk_dl = _spk_dl_tmp
                _dl.append(_spk_dl)  
                _spkl.append([i for _ in _spk_dl])
            assert len(_dl) == len(_spkl)
            return _dl, _spkl

        def _get_spk(folder):
            _spkl = sorted(glob(os.path.join(folder, '*')))
            print(f'_spkl {len(_spkl)}')
            print(f'speakers {self.hparams.data.speakers}')
            assert len(_spkl) == self.hparams.data.speakers
            return _spkl

        self.data_list, self.label_list = \
            _get_datalist(self.dataformat, _get_spk(self.directory))
        self.len_spk = [len(s) for s in self.data_list]
        self.index_spk = [0 for s in self.data_list]

        assert len(self.data_list) == len(self.label_list)
        assert len(self.data_list) != 0, "no data found at %s" % self.directory

    def __len__(self):
        return len(self.data_list)

    def __getitem__(self, index):
        if self.index_spk[index] == 0:
            random.shuffle(self.data_list[index])

        wav_list = []
        for i in range(self.index_spk[index], self.index_spk[index] + self.hparams.proto.shot):
            wavpath = self.data_list[index][i]
            wav, _ = librosa.load(wavpath, sr=self.hparams.audio.sampling_rate, mono=True)
            assert wav.shape[0] >= self.hparams.audio.input_t, \
                'audio length is too short: %s' % wavpath

            start = random.randint(0, wav.shape[0] - self.hparams.audio.input_t)
            wav = wav[start:start+self.hparams.audio.input_t]

            label = self.label_list[index][i]
            wav_list.append(torch.from_numpy(wav))

        wav_list = torch.stack(wav_list, dim=0)

        if self.index_spk[index] + self.hparams.proto.shot > self.len_spk[index]:
            self.index_spk[index] = 0
        else:
            self.index_spk[index] += self.hparams.proto.shot
        
        return wav_list, [index, index]

def create_vox_verif_dataloader(hparams): #only for test
    def collate_fn(batch):
        for label, wav1, wav2 in batch:
            label_list = torch.tensor([label])
            wav1 = torch.tensor(wav1).unsqueeze(0)
            wav2 = torch.tensor(wav2).unsqueeze(0)
        return label_list, wav1, wav2

    return DataLoader(dataset=Voxceleb_Verif_Dataset(hparams),
                  batch_size=1,
                  shuffle=False,
                  num_workers=hparams.train.num_workers,
                  collate_fn=collate_fn,
                  pin_memory=False,
                  sampler=None)

# only test
class Voxceleb_Verif_Dataset(Dataset):
    def __init__(self, hparams):
        #voxceleb hierarchy
        # voxceleb2(or 1)/id###/####/###.wav
        self.hparams = hparams
        with open(self.hparams.data.verification_list, 'r') as f:
            self.pair_list = [n.split() for n in f] 
            #1 id10270/x6uYqmx31kE/00001.wav id10270/8jEAjG6SegY/00008.wav
        self.directory = self.hparams.data.directory

        assert len(self.pair_list) != 0 , "no data found"

    def __len__(self):
        return len(self.pair_list)

    def __getitem__(self, index):
        label = int(self.pair_list[index][0])
        wavpath1 = os.path.join(self.hparams.data.verif_directory, self.pair_list[index][1])
        wavpath2 = os.path.join(self.hparams.data.verif_directory, self.pair_list[index][2])
        wav1, _ = librosa.load(wavpath1, sr=self.hparams.audio.sampling_rate, mono=True)
        wav2, _ = librosa.load(wavpath2, sr=self.hparams.audio.sampling_rate, mono=True)

        return label, wav1, wav2


def create_voxsrc_dataloader(hparams, data_path): #only for test
    def collate_fn(batch):
        for name, wav1 in batch:
            name = name
            wav1 = torch.tensor(wav1).unsqueeze(0)
        return name, wav1
    return DataLoader(dataset=Voxsrc_Dataset(hparams, data_path),
                  batch_size=1,
                  shuffle=False,
                  num_workers=hparams.train.num_workers,
                  collate_fn=collate_fn,
                  pin_memory=False,
                  sampler=None)

# only test
class Voxsrc_Dataset(Dataset):
    def __init__(self, hparams, data_path):
        #voxceleb hierarchy
        # voxceleb2(or 1)/id###/####/###.wav
        self.hparams = hparams
        self.directory = data_path
        self.wavlist = glob(os.path.join(data_path, '*.wav'))

    def __len__(self):
        return len(self.wavlist)

    def __getitem__(self, index):
        wav1, _ = librosa.load(self.wavlist[index], sr=self.hparams.audio.sampling_rate, mono=True)
        name = self.wavlist[index]
        return name, wav1
