# brain_sv_acm

ACM speaker verification

- `brain_idl` version: 1.0.0
- 관련 컨플루언스 글: https://pms.maum.ai/confluence/x/G2mqAQ
- This uses `docker.maum.ai:443/jun3518:base-20.07.22`, which is documented at https://pms.maum.ai/confluence/x/uBuqAQ.

## Build
```bash
docker build -f Dockerfile -t sv_acm:<version>: .
docker build -f Dockerfile-server --build-arg sv_acm_version=<version> -t sv_acm:<version>-server .
```

## Author

브레인 이준혁 연구원, 박승원 수석
