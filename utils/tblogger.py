import slack
import tempfile
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import torch
import torch.nn.functional as F

from os import path, makedirs
from omegaconf import OmegaConf as OC
from datetime import datetime, timedelta
from pytorch_lightning.loggers import TensorBoardLogger
from tensorboard_reporter.loader import load_summaries, group_by_tag
from tensorboard_reporter.summary import tags, wall_time_in_range
from tensorboard_reporter.stats import current_mean


class TensorBoardLoggerExpanded(TensorBoardLogger):
    def __init__(self, save_dir, hparams):
        super(TensorBoardLoggerExpanded, self).__init__(save_dir, name=hparams.name)
        self.hparams = hparams
        self.log_hyperparams(OC.to_container(hparams))
        self.epoch = 0
        self.slack_bot_token = "xoxb-932600564919-1257929981203-FdNTVOv84YLyiiS3tDzAaldq"
        self.slack_channel = "@U0104L9EB0E"
        self.last_time_stamp = datetime.now()
        self.tags_to_report = ['eer', 'thres', 'epoch','acc', 'loss']

    def reporter(self):
        client = slack.WebClient(token=self.slack_bot_token)
        self.experiment.flush()
        summaries = load_summaries(self.log_dir)
        summaries = filter(tags(self.tags_to_report), summaries)
        summaries = filter(
            wall_time_in_range(
                min=self.last_time_stamp, max=None
            ),
            summaries,
        )
        self.last_time_stamp = datetime.now()

        summaries_by_tag = group_by_tag(summaries)
             
        fig, axs = plt.subplots(len(self.tags_to_report), sharex=True)

        stats_by_tag = dict()

        for i, (tag, summaries) in enumerate(summaries_by_tag.items()):
            ax = axs[i]
            ax.set_title(tag)

            steps = []
            values = []
            for summary in summaries:
                steps.append(summary.step)
                values.append(summary.value)

            values_np = np.array(values)

            stats_by_tag[tag] = dict(
                mean=values_np.mean(), min=values_np.min(), max=values_np.max()
            )
            ax.plot(steps, values)

        summary_text = ""
        for tag, stats in stats_by_tag.items():
            summary_text += "*{}*, min=*{:.3f}*, max=*{:.3f}*, mean=*{:.3f}* \n".format(
                tag, stats["min"], stats["max"], stats["mean"]
            )
            if tag == "epoch":
                current_epoch = stats["max"]

        with tempfile.NamedTemporaryFile() as file:
            fig.savefig(file.name, format="png")
            
            client.files_upload(
                channels=self.slack_channel,
                file=file.name,
                title="Report_{}".format(f'{self.name}_epoch_{current_epoch}'),
                initial_comment=summary_text,
            )
            plt.cla()
            plt.close(fig)
        del fig
        del summaries
        del client
        del summaries_by_tag
        del stats_by_tag
