import torch
import torch.nn as nn


class SpecAug(nn.Module):
    def __init__(self):
        super().__init__()

    @torch.no_grad()
    def forward(self, x, p=0.1, f=0.1, t=0.1):
        B, F, T = x.shape
        mask = torch.ones_like(x, device=x.device)
        rand = torch.rand(B) < p
        for i in range(B):
            if rand[i]:
                # frequency mask
                mask[i,
                    torch.randint(0, F, (torch.randint(0, int(F*f), (1, )), )),
                    :] = 0. 
                # time mask
                mask[i,
                    :,
                    torch.randint(0, T, (torch.randint(0, int(T*t), (1, )), ))] = 0. 
        x = x*mask
        return x.detach()
