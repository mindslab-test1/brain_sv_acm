import torch
import torch.nn as nn


class Comp(nn.Module):
    def __init__(self, max_amp, ratio, threshold):
        super().__init__()
        self.ratio = ratio
        self.threshold = threshold
        self.max_amp = max_amp

    @torch.no_grad()
    def forward(self, x):
        noise = x.mean(dim=0)
        noise = noise * torch.clamp(1 / noise.abs().max(), max = 10.)
        noise = noise - (1 - self.ratio) * (noise.abs() - self.threshold) * noise.sign()
        x = x + self.max_amp * noise.unsqueeze(0)
        return x.detach()
