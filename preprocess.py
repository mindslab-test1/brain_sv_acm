import os
import glob
import tqdm
import torch
import librosa
import argparse
import datetime
from omegaconf import OmegaConf as OC

from lightning_model import ACMSV


def main(args):
    hparams = OC.load(args.config)
    now = datetime.datetime.now().strftime("%m_%d_%H")
    hparams.name = f'global_{now}'

    os.makedirs(hparams.log.tensorboard_dir, exist_ok=True)

    model = ACMSV(hparams, train=False).cuda()
    base = torch.load(args.model)
    model.load_state_dict(base['state_dict'], strict=False)
    model.eval()
    model.freeze()
    del base

    wavlist = glob.glob(os.path.join(args.root_dir, '**', '*.wav'), recursive=True)

    if len(wavlist) == 0:
        print('No wav file found under path %s' % args.root_dir)

    for wavpath in tqdm.tqdm(wavlist, desc='Audio sanity check'):
        wav, sampling_rate = librosa.load(wavpath, sr=None)
        assert sampling_rate == hparams.audio.sampling_rate, \
            'Sampling rate mismatch at %s: expected %d, got %d' \
            % (wavpath, hparams.audio.sampling_rate, sampling_rate)

    for wavpath in tqdm.tqdm(wavlist, desc='Pre-computing speaker vectors'):
        wav, sampling_rate = librosa.load(wavpath, sr=None)
        wav = torch.from_numpy(wav).cuda()
        wav = wav.unsqueeze(0)
        result = model.inference(wav)
        result = result.view(-1).cpu().detach()

        pt_path = wavpath.replace('.wav', '_vec.pt')
        torch.save(result, pt_path)

    print('Pre-computed and saved speaker vectors for %d audios under path %s' %
        (len(wavlist), args.root_dir))


if __name__ =='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, default='config/default.yaml',
                        help="Configuration yaml path")
    parser.add_argument('-m', '--model', type=str, required=True,
                        help="Checkpoint(model) path")
    parser.add_argument('-r', '--root_dir', type=str, required=True,
                        help="Root directory of audios to be pre-processed")
    args = parser.parse_args()
    main(args)
