import torch
import torch.nn as nn
import torch.nn.functional as F


class CrossEntropyLoss(nn.Module):
    def __init__(self,
            num_classes,
            epsilon=0.1,
            label_smooth=True,
            eps_min=0.01,
            eps_max=0.99,
            temp = 1.):
        super(CrossEntropyLoss, self).__init__()
        self.num_classes = num_classes
        self.epsilon = epsilon
        self.eps_min = eps_min
        self.eps_max = eps_max
        self.softmax = nn.Softmax(dim=1)
        self.temp = temp

    def forward(self, inputs, targets):
        """
        Args:
            inputs (torch.Tensor): prediction matrix (before softmax) with
                shape (batch_size, num_classes).
            targets (torch.LongTensor): ground truth labels with shape (batch_size).
                Each position contains the label index.
        """
        probs = self.softmax(inputs)
        log_probs = torch.log(probs)
        zeros = torch.zeros(log_probs.size(), device=inputs.device)
        targets = zeros.scatter_(1, targets.unsqueeze(1), 1)
        targets = (1 - self.epsilon) * targets + \
                self.epsilon * self.softmax(inputs / self.temp).detach()

        return (-targets * log_probs).sum(1).mean()
