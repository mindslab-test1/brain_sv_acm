import torch
import torch.nn as nn


class SEBasicBlock(nn.Module):
    expansion = 1

    def __init__(self,
                 inplanes,
                 planes,
                 stride=1,
                 downsample=None,
                 reduction=8):
        super(SEBasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(inplanes,
                               planes,
                               kernel_size=3,
                               stride=stride,
                               padding=1,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes,
                               planes,
                               kernel_size=3,
                               padding=1,
                               bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.se = SELayer(planes, reduction)
        self.downsample = downsample
        self.stride = stride

    def get_bn(self):
        return self.bn2

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.relu(out)
        out = self.bn1(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.se(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        return out


class SEBottleneck(nn.Module):
    expansion = 4

    def __init__(self,
                 inplanes,
                 planes,
                 stride=1,
                 downsample=None,
                 reduction=8):
        super(SEBottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes,
                               planes,
                               kernel_size=3,
                               stride=stride,
                               padding=1,
                               bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * 4, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * 4)
        self.relu = nn.ReLU(inplace=True)
        self.se = SELayer(planes * 4, reduction)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)
        out = self.se(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual

        return out


class SELayer(nn.Module):
    def __init__(self, channel, reduction=8):
        super(SELayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(nn.Linear(channel, channel // reduction),
                                nn.ReLU(inplace=True),
                                nn.Linear(channel // reduction, channel),
                                nn.Sigmoid())

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1, 1)
        return x * y


class ACMBasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None, group=8):
        super(ACMBasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(inplanes,
                               planes,
                               kernel_size=3,
                               stride=stride,
                               padding=1,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes,
                               planes,
                               kernel_size=3,
                               padding=1,
                               bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.acm = ACMLayer(planes, group)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        if type(x)==tuple:
            x ,orth_sum = x
        else:
            orth_sum =0

        residual = x

        out = self.conv1(x)
        out = self.relu(out)
        out = self.bn1(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out, orth= self.acm(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        orth_sum +=orth
        return out, orth_sum


class ACMBottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None, group=8):
        super(ACMBottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes,
                               planes,
                               kernel_size=3,
                               stride=stride,
                               padding=1,
                               bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes * 4, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes * 4)
        self.relu = nn.ReLU(inplace=True)
        self.acm = ACMLayer(planes * 4, group)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        if type(x)==tuple:
            x ,orth_sum = x
        else:
            orth_sum =0
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)
        out, orth = self.acm(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        orth_sum +=orth
        return out, orth_sum


class ACMLayer(nn.Module):
    def __init__(self, channel, group=32):
        super(ACMLayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(nn.Linear(channel, channel // group),
                                nn.ReLU(inplace=True),
                                nn.Linear(channel // group, channel),
                                nn.Sigmoid())
        self.mu = nn.Sequential(
            nn.Conv2d(channel, channel // 2, 1, groups=group), nn.ReLU(),
            nn.Conv2d(channel // 2, channel, 1, groups=group), nn.Sigmoid())
        self.key_conv = nn.Conv2d(channel, group, 1, groups=group)
        self.query_conv = nn.Conv2d(channel, group, 1, groups=group)
        self.softmax = nn.Softmax(dim = -1)
        self.cos = nn.CosineSimilarity(dim =1)
        self.group = group
        self.c_per_g = channel//group
    def forward(self, x):
        b, c, w, h = x.shape
        m = self.avg_pool(x)
        p = self.mu(m)
        normal = x - m
        key = self.softmax(self.key_conv(normal).view(b * self.group, 1,
                                                      -1)).transpose(1, 2)
        key = torch.bmm(normal.view(b * self.group, self.c_per_g, -1),
                        key).view(b, c, 1, 1)
        query = self.softmax(
            self.query_conv(normal).view(b * self.group, 1,
                                         -1)).transpose(1, 2)
        query = torch.bmm(normal.view(b * self.group, self.c_per_g, -1),
                          query).view(b, c, 1, 1)
        x = (x+key-query)*p
        orth = nn.functional.relu(self.cos(key, query)).mean()
        return x, orth
