import torch
import torch.nn as nn
import torch.nn.functional as F

from .cross_entropy import CrossEntropyLoss


class AngleProtoLoss(nn.Module):
    def __init__(self, hparams):
        super(AngleProtoLoss, self).__init__()
        self.hparams = hparams
        self.w = nn.Parameter(torch.tensor(10.))
        self.b = nn.Parameter(torch.tensor(-5.))
        self.bce = nn.BCELoss()
        self.criterion = CrossEntropyLoss(self.hparams.proto.step,
                                          epsilon=self.hparams.proto.smoothing,
                                          label_smooth=True,
                                          eps_min=self.hparams.proto.eps_min,
                                          eps_max=self.hparams.proto.eps_max)

    def accuracy(self, pred, target):
        _, top5i = torch.topk(pred, k=1, dim=1)  #change k to 5 if u want top5
        acc = torch.eq(target, top5i[:, 0]).to(torch.float).mean()
        #acc5 = i.view(-1,1).expand_as(top5i).eq(top5i).sum(dim=1).to(torch.float).mean()
        return acc  #acc5

    def forward(self, x):
        x = x.view(self.hparams.proto.step, self.hparams.proto.shot, -1)
        out_anchor = torch.mean(x[:, 1:, :], 1)
        out_positive = x[:, 0, :]
        cos_sim_matrix = F.linear(out_positive, out_anchor)
        torch.clamp(self.w, 1e-6)
        cos_sim_matrix = cos_sim_matrix * self.w + self.b
        label = torch.arange(self.hparams.proto.step, device=x.device)
        nloss = self.criterion(cos_sim_matrix, label)
        prec1 = self.accuracy(cos_sim_matrix, label)
        return nloss, prec1
