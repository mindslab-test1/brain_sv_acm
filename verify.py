import os
import torch
import argparse
import datetime

from omegaconf import OmegaConf as OC
from pytorch_lightning import Trainer

from lightning_model import ACMSV


def main(args):
    hparams = OC.load(args.config)
    now = datetime.datetime.now().strftime("%m_%d_%H")
    hparams.name = f'global_{now}'

    os.makedirs(hparams.log.tensorboard_dir, exist_ok=True)

    model = ACMSV(hparams, train=False)
    base = torch.load(args.checkpoint_path)
    model.load_state_dict(base['state_dict'], strict=False)
    model.eval()
    model.freeze()
    del base

    trainer = Trainer(logger=False, gpus=1)
    trainer.test(model)


if __name__ =='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="Configuration yaml path")
    parser.add_argument('-p', '--checkpoint_path', type=str, required=True,
                        help="Checkpoint path")
    args = parser.parse_args()
    main(args)
