import time
import grpc
import torch
import logging
import argparse
from concurrent import futures
from scipy.io.wavfile import read
from omegaconf import OmegaConf as OC

from lightning_model import ACMSV
from utils.utils import wav_formatter
from utils.wav_wrapper import WavBinaryWrapper

import dap_pb2
from dap_pb2_grpc import GhostVLADServicer, add_GhostVLADServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class SVACMServicer(GhostVLADServicer):
    def __init__(self, checkpoint_path, device, config_path):
        super().__init__()
        torch.cuda.set_device(device)
        self.device = device

        self.hparams = OC.load(config_path)
        self.model = ACMSV(self.hparams, train=False).to(device)
        self._load_checkpoint(checkpoint_path)

        self.emb_config = dap_pb2.EmbConfig(
            emb_dim=self.hparams.proto.nOut)

    def _load_checkpoint(self, checkpoint_path):
        checkpoint = torch.load(checkpoint_path, map_location='cpu')
        self.model.load_state_dict(checkpoint['state_dict'], strict=False)
        self.model.eval()
        self.model.freeze()
        del checkpoint
        torch.cuda.empty_cache()

    @torch.no_grad()
    def GetDvectorFromWav(self, wav_binary_iterator, context):
        torch.cuda.set_device(self.device)
        try:
            wav = bytearray()
            for wav_binary in wav_binary_iterator:
                wav.extend(wav_binary.bin)
            wav = WavBinaryWrapper(wav)

            sampling_rate, wav = read(wav)
            if sampling_rate != self.hparams.audio.sampling_rate:
                context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                context.set_details("Sampling rate mismatch: expected %d, got %d" % \
                    (self.hparams.audio.sampling_rate, sampling_rate))

            wav = wav_formatter(wav)

            wav = torch.from_numpy(wav).cuda()
            wav = wav.unsqueeze(0)
            result = self.model.inference(wav)
            result = result.float().view(-1).cpu().detach().tolist()
            result = dap_pb2.Embedding(data=result)
            return result

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def GetEmbConfig(self, empty, context):
        return self.emb_config


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='ACM based speaker verification executor')
    parser.add_argument('-c', '--config', type=str, default='config/default.yaml',
                        help="path of configuration yaml file")
    parser.add_argument('-m', '--model', type=str, required=True,
                        help="path of checkpoint(model)")
    parser.add_argument('-l', '--log_level', type=str, default='INFO',
                        help="logger level")
    parser.add_argument('-p', '--port', type=int, default=43001,
                        help="grpc port")
    parser.add_argument('-d', '--device', nargs='?', type=int, default=0,
                        help="gpu device")
    parser.add_argument('-w', '--max_workers', type=int, default=1,
                        help="max workers")
    args = parser.parse_args()

    servicer = SVACMServicer(args.model, args.device, args.config)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers), )
    add_GhostVLADServicer_to_server(servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('SVACM(brain_sv_acm) starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
